import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  Name: any;
  gender: any;
  country: any;
  emailId: any;
  password: any;
  
  constructor() {
  }

  ngOnInit() {
  }

  submit() {
    console.log("Name: " + this.Name);
    console.log("Gender: " + this.gender);
    console.log("Country: " + this.country);
    console.log("Email-Id: " + this.emailId);
    console.log("Password: " + this.password);
  }

}
