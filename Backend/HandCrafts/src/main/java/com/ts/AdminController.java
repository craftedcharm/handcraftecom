package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AdminDao;
import com.model.Admin;

@RestController
public class AdminController {
	//Implementing Dependency Injection for ProductDao 
		@Autowired
		AdminDao adminDao;
		
		@GetMapping("getAllAdmins")
		public List<Admin> getAllAdmins() {		
			return adminDao.getAllAdmins();
		}
		

}
