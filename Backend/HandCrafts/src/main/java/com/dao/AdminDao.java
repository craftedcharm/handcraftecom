package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Admin;

@Service
public class AdminDao {

	//Implementing Dependency Injection for ProductRepository
	@Autowired
	AdminRepository adminRepository;

	public List<Admin> getAllAdmins() {
		return adminRepository.findAll();
	}
	

}
