package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    private int adminId;
    private String adminName;
    private String emailId;
    private String password;

    public Admin() {
    }

    public Admin(String adminName, String emailId, String password) {
        this.adminName = adminName;
        this.emailId = emailId;
        this.password = password;
    }

    public Admin(int adminId, String adminName, String emailId, String password) {
        this.adminId = adminId;
        this.adminName = adminName;
        this.emailId = emailId;
        this.password = password;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public String toString() {
        return "Admin [adminId=" + adminId + ", adminName=" + adminName + ", emailId=" + emailId + ", password=" + password + "]";
    }

}
